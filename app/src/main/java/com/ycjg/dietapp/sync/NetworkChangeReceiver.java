package com.ycjg.dietapp.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ycjg.dietapp.utils.NetworkUtils;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by yragalvez on 08/11/2018.
 * DietApp
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    private Subject<Boolean> stream = PublishSubject.create();

    @Override
    public void onReceive(Context context, Intent intent) {
        stream.onNext(NetworkUtils.isOnline(context));
    }

    public Observable<Boolean> getStream() {
        return stream;
    }
}
