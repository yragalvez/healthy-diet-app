package com.ycjg.dietapp.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public final class CommonUtils {

    private CommonUtils() {}

    public static ProgressBar showProgressBar(Context context, @NonNull LinearLayout rootView) {
        ProgressBar progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setIndeterminate(true);
        rootView.addView(progressBar);

        return progressBar;
    }
}
