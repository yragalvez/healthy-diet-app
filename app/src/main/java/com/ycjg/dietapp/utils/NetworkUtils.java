package com.ycjg.dietapp.utils;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.ycjg.dietapp.sync.NetworkChangeReceiver;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 08/11/2018.
 * DietApp
 */
public final class NetworkUtils {
    private static NetworkUtils instance;
    private NetworkChangeReceiver receiver;

    private NetworkUtils() {}

    public static NetworkUtils get() {
        if (instance == null)
            instance = new NetworkUtils();

        return instance;
    }

    public void registerNetworkChangeReceiver(Context context) {
        receiver = new NetworkChangeReceiver();
        IntentFilter filter = new IntentFilter("android.net.CONNECTIVITY_CHANGE");
        context.registerReceiver(receiver, filter);
    }

    public Observable<Boolean> getNetworkChangeStream() {
        if (receiver != null)
            return receiver.getStream();
        return null;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
