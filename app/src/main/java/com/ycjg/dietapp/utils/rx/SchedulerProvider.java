package com.ycjg.dietapp.utils.rx;

import io.reactivex.Scheduler;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public interface SchedulerProvider {

    Scheduler ui();

    Scheduler computation();

    Scheduler io();
}
