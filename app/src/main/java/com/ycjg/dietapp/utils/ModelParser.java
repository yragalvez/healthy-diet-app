package com.ycjg.dietapp.utils;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yragalvez on 15/11/2018.
 * DietApp
 */
public class ModelParser<T> {

    private Class<T> cl;

    public ModelParser(Class<T> cl) {
        this.cl = cl;
    }

    public T parse(JSONObject json) {
        if (json == null)
            return null;
        try {
            return new ObjectMapper().readValue(json.toString(), cl);
        } catch (IOException ex) {
            Log.e(cl.getSimpleName(), "parse: ", ex);
            return null;
        }
    }

    public List<T> parseList(JSONArray array) {
        List<T> list = new ArrayList<>();
        try {
            for (int i = 0; i < array.length(); i++) {
                T model = parse(array.getJSONObject(i));
                if (model != null)
                    list.add(model);
            }
            return list;
        } catch (JSONException ex) {
            Log.e(cl.getSimpleName(), "parse: ", ex);
            return list;
        }
    }

    public List<T> parseList(JSONObject json) {
        try {
            return parseList(json.getJSONArray("data"));
        } catch (JSONException ex) {
            return new ArrayList<>();
        }
    }
}
