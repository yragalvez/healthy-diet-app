package com.ycjg.dietapp.utils;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by yragalvez on 08/11/2018.
 * DietApp
 */
public final class Utils {

    private Utils() {}

    public static boolean isTextInvalid(String content) {
        return TextUtils.isEmpty(content) && (content.length() < 4);
    }

    public static boolean isTextValid(String text) {
        return !TextUtils.isEmpty(text) && text.length() > 1;
    }

    public static boolean isValidName(String name) {
        String namePattern = "[a-zA-Z\\s]+";
        if (name == null || name.isEmpty())
            name = "";
        return name.trim().matches(namePattern);
    }

    public static boolean isValidEmail(String email) {
        String emailPattern = Patterns.EMAIL_ADDRESS.pattern();
        if (email == null || email.isEmpty())
            email = "";
        return email.trim().matches(emailPattern);
    }

    public static boolean isValidContactNumber(String phoneNumber) {
        String phonePattern = Patterns.PHONE.pattern();
        if (phoneNumber == null || phoneNumber.isEmpty())
            phoneNumber = "";
        return phoneNumber.trim().matches(phonePattern);
    }

    /**
     * Formula for BMI:
     * Metric BMI formula = weight (kg) / height^2 (m^2)
     * Imperial BMI formula = weight (lb) / height^2 (in^2) * 703
     * */
    public static double computeBmiMetric(double weight, double height, boolean isImperial) {
        double userBMI = weight / (height * height);
        if (isImperial)
            userBMI *= 703;
        return userBMI;
    }
}
