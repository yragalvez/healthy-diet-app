package com.ycjg.dietapp.utils;

import com.ycjg.dietapp.BuildConfig;

/**
 * Created by yragalvez on 12/11/2018.
 * DietApp
 */
public final class AppConstants {

    public static final String DB_NAME = "dietapp.db";
    public static final String PREF_NAME = "DietAppPref";

    public static final String ACCOUNT_TYPE = BuildConfig.ACCOUNT_TYPE;
    public static final String GENERAL_KEY_TYPE = "User";
    public static final String USER_INFO_KEY = "UserInfo";

    public static final String EXITING_TAG = "is_exiting";
    public static final String DRAWER_TAG = "open_drawer";

    public static final int DEFAULT_INT = -1;
    public static final String DEFAULT_STRING = "";
    public static final boolean DEFAULT_STATUS = false;

    private AppConstants() {}
}
