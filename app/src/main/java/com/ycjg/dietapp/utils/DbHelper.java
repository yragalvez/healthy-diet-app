package com.ycjg.dietapp.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by yragalvez on 09/11/2018.
 * DietApp
 */
public final class DbHelper {
    private static final String TAG = DbHelper.class.getSimpleName();

    private DbHelper() {}

    public void initializeDatabase() {

    }

    public static String toJson(Object value) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
