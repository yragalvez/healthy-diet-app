package com.ycjg.dietapp.ui.diet;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.ui.base.MvpView;

/**
 * Created by yragalvez on 14/11/2018.
 * DietApp
 */
public interface DietMvpView extends MvpView {
    void setUserDataHeader(User user);

    void showUserProgressLayout(boolean hasPlan);

    void navigateToBmi();
}
