package com.ycjg.dietapp.ui.info;

import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.ui.base.MvpPresenter;

/**
 * Created by yragalvez on 22/11/2018.
 * DietApp
 */
@PerActivity
public interface InformationMvpPresenter<V extends InformationMvpView,
        I extends InformationMvpInteractor> extends MvpPresenter<V, I> {
    void computeBMI(String weight, String height, boolean isImperial);

    void setupShownScreen();

    void setBmiButtonAction();
}
