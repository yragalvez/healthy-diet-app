package com.ycjg.dietapp.ui.info;

import com.ycjg.dietapp.ui.base.BasePresenter;
import com.ycjg.dietapp.utils.AppLogger;
import com.ycjg.dietapp.utils.Utils;
import com.ycjg.dietapp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.ycjg.dietapp.ui.info.InformationActivity.TEXT_HEIGHT_ERROR;
import static com.ycjg.dietapp.ui.info.InformationActivity.TEXT_WEIGHT_ERROR;

/**
 * Created on 22/11/2018.
 * DietApp
 */
public class InformationPresenter<V extends InformationMvpView, I extends InformationMvpInteractor>
        extends BasePresenter<V, I> implements InformationMvpPresenter<V, I> {

    private static final String TAG = InformationPresenter.class.getSimpleName();

    @Inject
    public InformationPresenter(I mvpInteractor,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void computeBMI(String weight, String height, boolean isImperial) {
        if (!isViewAttached()) return;

        getMvpView().hideKeyboard();

        boolean isValidWeight = Utils.isTextValid(weight);
        boolean isValidHeight = Utils.isTextValid(height);

        showErrorMessages(isValidWeight, isValidHeight);

        try {
            if (isValidWeight && isValidHeight) {
                double dWeight = Double.parseDouble(weight);
                double dHeight = Double.parseDouble(height);

                if (!isImperial) dHeight /= 100;

                double bmiValue = Utils.computeBmiMetric(dWeight, dHeight, isImperial);

                getMvpView().setupBmiValue(bmiValue);

                if (!isImperial)
                    dHeight *= 100;
                updateUser(dWeight, dHeight, isImperial);
            }
        } catch (NumberFormatException e) {
            AppLogger.e(TAG, "computeBMI", e);
        }
    }

    private void showErrorMessages(boolean isValidWeight, boolean isValidHeight) {
        getMvpView().clearFieldErrors();
        if (!isValidWeight)
            getMvpView().setFieldRequiredError(TEXT_WEIGHT_ERROR);
        if (!isValidHeight)
            getMvpView().setFieldRequiredError(TEXT_HEIGHT_ERROR);
    }

    private void updateUser(final double dWeight, final double dHeight, boolean isImperial) {
        getCompositeDisposable().add(getInteractor()
                .getUser()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(user -> {
                    if (user != null) {
                        user.setHeight(dHeight);
                        user.setWeight(dWeight);
                        getInteractor().updateUser(user, isImperial);
                        getInteractor().setFirstLoginToFalse();
                    }
                }, throwable -> AppLogger.e(TAG, "computeBMI()", throwable)));
    }

    @Override
    public void setupShownScreen() {
        String sHeight = getInteractor().getUserHeight();
        String sWeight = getInteractor().getUserWeight();

        if (!isViewAttached()) return;

        getMvpView().setPlanButtonText(getInteractor().getUserPlan() < 0);
        if (getInteractor().isFirstLogin())
            getMvpView().showBmiComputation(true);
        else if (Utils.isTextValid(sHeight) && Utils.isTextValid(sWeight)) {
            getMvpView().showBmiComputation(false);
            computeBMI(sWeight, sHeight, getInteractor().isImperial());
        }
    }

    @Override
    public void setBmiButtonAction() {
        if (!isViewAttached()) return;

        if (getInteractor().getUserPlan() < 0)
            getMvpView().choosePlan();
        else getMvpView().showBmiComputation(true);
    }
}
