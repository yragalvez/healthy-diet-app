package com.ycjg.dietapp.ui.register;

import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.ui.base.MvpPresenter;

/**
 * Created by yragalvez on 15/11/2018.
 * DietApp
 */
@PerActivity
public interface RegistrationMvpPresenter<V extends RegistrationMvpView, I extends RegistrationMvpInteractor>
        extends MvpPresenter<V, I> {

    void registerUser(String userName, String password, String retypedPassword, String contactNumber, String email);
}
