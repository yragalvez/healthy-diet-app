package com.ycjg.dietapp.ui.register;

import com.ycjg.dietapp.ui.base.MvpInteractor;

/**
 * Created by yragalvez on 15/11/2018.
 * DietApp
 */
public interface RegistrationMvpInteractor extends MvpInteractor {
}
