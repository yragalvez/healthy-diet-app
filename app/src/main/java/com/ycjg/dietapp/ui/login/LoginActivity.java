package com.ycjg.dietapp.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ycjg.dietapp.BuildConfig;
import com.ycjg.dietapp.R;
import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.ui.base.BaseAuthActivity;
import com.ycjg.dietapp.ui.main.MainActivity;
import com.ycjg.dietapp.ui.register.RegistrationActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseAuthActivity implements LoginMvpView {

    @Inject
    LoginMvpPresenter<LoginMvpView, LoginMvpInteractor> mPresenter;

    @BindView(R.id.etEmail)
    protected EditText etEmail;

    @BindView(R.id.etPassword)
    protected EditText etPassword;

    @BindView(R.id.llLoginForm)
    protected LinearLayout mLoginFormView;

    @BindView(R.id.llProgressBar)
    protected LinearLayout mProgressBar;

    @BindView(R.id.email_login_form)
    protected LinearLayout mLogin;

    @BindView(R.id.btnSignIn)
    protected Button btnSignIn;

    @BindView(R.id.btnRegister)
    protected Button btnRegister;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(LoginActivity.this);

        setUp();
    }

    @Override
    protected void setUp() {
        if (BuildConfig.DEBUG) {
            etEmail.setText("foo@example.com");
            etPassword.setText("hello");
        }
    }

    @OnEditorAction(R.id.etPassword)
    boolean onPasswordLogin(TextView textView, int id, KeyEvent keyEvent) {
        if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
            attemptLogin();
            return true;
        }
        return false;
    }

    @OnClick(R.id.btnSignIn)
    void onSignIn() {
        attemptLogin();
    }

    @OnClick(R.id.btnRegister)
    void onRegister() {
        register();
    }

    private void register() {
        startActivity(RegistrationActivity.getStartIntent(this));
    }

    private void attemptLogin() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        mPresenter.onServerLoginClick(email, password);
    }

    @Override
    public void setEmailError(String error) {
        etEmail.setError(error);
    }

    @Override
    public void setEmailError(@StringRes int resId) {
        setEmailError(getResources().getString(resId));
    }

    @Override
    public void setPasswordError(String error) {
        etPassword.setError(error);
    }

    @Override
    public void setPasswordError(@StringRes int resId) {
        setPasswordError(getResources().getString(resId));
    }

    @Override
    public void openMainActivity() {
        MainActivity.goToMain(this);
    }

    @Override
    public void onSuccessfulLogin(User user, String password) {
        Intent intent = mPresenter.addToAccountManager(user, password);
        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);
        finish();
        openMainActivity();
    }

    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        showLoading(mProgressBar);
        mLogin.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}

