package com.ycjg.dietapp.ui.diet;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.data.db.WeightPlan.Weight;
import com.ycjg.dietapp.data.db.repository.UserRepository;
import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.ui.base.BaseInteractor;
import com.ycjg.dietapp.utils.AppConstants;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 14/11/2018.
 * DietApp
 */
public class DietInteractor extends BaseInteractor
        implements DietMvpInteractor {

    private UserRepository mUserRepo;

    @Inject
    public DietInteractor(PreferencesHelper preferencesHelper,
                          UserRepository userRepository) {
        super(preferencesHelper);
        mUserRepo = userRepository;
    }

    @Override
    public Observable<User> getUser() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return mUserRepo.getUser(email);
        return null;
    }

    @Override
    public int getUserPlan() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return getPreferencesHelper().getUserPlan(email);
        return AppConstants.DEFAULT_INT;
    }

    @Override
    public void setUserPlan(@Weight int plan) {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            getPreferencesHelper().setUserPlan(email, plan);
    }
}
