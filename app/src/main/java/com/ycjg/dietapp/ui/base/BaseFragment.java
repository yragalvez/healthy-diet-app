package com.ycjg.dietapp.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ycjg.dietapp.di.component.ActivityComponent;
import com.ycjg.dietapp.utils.CommonUtils;

import butterknife.Unbinder;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public abstract class BaseFragment extends Fragment implements MvpView {

    private BaseActivity mActivity;
    private Unbinder mUnbinder;
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUp(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }
    }

    @Override
    public void showLoading(LinearLayout rootView) {
        hideLoading();
        mProgressBar = CommonUtils.showProgressBar(this.getContext(), rootView);
    }

    @Override
    public void hideLoading() {
        if (mProgressBar != null && mProgressBar.isShown())
            mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(@StringRes int resId) {
        if (mActivity != null)
            mActivity.onError(resId);
    }

    @Override
    public void onError(String message) {
        if (mActivity != null)
            mActivity.onError(message);
    }

    @Override
    public void showMessage(@StringRes int resId) {
        if (mActivity != null)
            mActivity.showMessage(resId);
    }

    @Override
    public void showMessage(String message) {
        if (mActivity != null)
            mActivity.showMessage(message);
    }

    @Override
    public boolean isNetworkConnected() {
        if (mActivity != null)
            mActivity.isNetworkConnected();
        return false;
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    @Override
    public void hideKeyboard() {
        if (mActivity != null)
            mActivity.hideKeyboard();
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null)
            return mActivity.getActivityComponent();
        return null;

    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public void setUnbinder(Unbinder unbinder) {
        mUnbinder = unbinder;
    }

    protected abstract void setUp(View view);

    @Override
    public void onDestroy() {
        if (mUnbinder != null)
            mUnbinder.unbind();
        super.onDestroy();
    }

    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }
}
