package com.ycjg.dietapp.ui.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ycjg.dietapp.R;
import com.ycjg.dietapp.ui.base.BaseActivity;
import com.ycjg.dietapp.ui.diet.DietActivity;
import com.ycjg.dietapp.ui.info.InformationActivity;
import com.ycjg.dietapp.utils.AppConstants;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainMvpView {

    @Inject
    MainMvpPresenter<MainMvpView, MainMvpInteractor> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(MainActivity.this);

        boolean isExiting = getIntent().getBooleanExtra(AppConstants.EXITING_TAG, false);
        if (isExiting) finish();
        else mPresenter.authenticateActivity();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.setupShownScreen();
    }

    @Override
    public void showMenus() {
        Intent i = DietActivity.getStartIntent(this);
        if (getIntent().hasExtra(AppConstants.DRAWER_TAG)) i.putExtra(AppConstants.DRAWER_TAG, true);
        startActivity(i);
    }

    @Override
    public void showComputeBmi() {
        Intent i = InformationActivity.getStartIntent(this);
        startActivity(i);
    }

    public static void goToMain(Context context) {
        goToMain(context, false);
    }

    public static void goToMain(Context context, boolean isExiting) {
        goToMain(context, isExiting, false);
    }

    public static void goToMain(Context context, boolean isExiting, boolean openDrawer) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(AppConstants.EXITING_TAG, isExiting);

        if (openDrawer) intent.putExtra(AppConstants.DRAWER_TAG, true);
        if (!(context instanceof Activity)) intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void setUp() {

    }
}
