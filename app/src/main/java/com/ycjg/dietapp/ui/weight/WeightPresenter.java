package com.ycjg.dietapp.ui.weight;

import com.ycjg.dietapp.ui.base.BasePresenter;
import com.ycjg.dietapp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yragalvez on 20/11/2018.
 * DietApp
 */
public class WeightPresenter<V extends WeightMvpView, I extends WeightMvpInteractor>
        extends BasePresenter<V, I> implements WeightMvpPresenter<V, I> {

    @Inject
    public WeightPresenter(I mvpInteractor,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }
}
