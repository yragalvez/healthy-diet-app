package com.ycjg.dietapp.ui.login;

import android.content.Intent;

import com.ycjg.dietapp.data.account_manager.AccountManagerHelper;
import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.data.db.repository.UserRepository;
import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.ui.base.BaseInteractor;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public class LoginInteractor extends BaseInteractor
        implements LoginMvpInteractor {

    private final AccountManagerHelper mAccountManager;
    private UserRepository mUserRepository;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    @Inject
    public LoginInteractor(PreferencesHelper preferencesHelper,
                           AccountManagerHelper accountManagerHelper,
                           UserRepository userRepository) {
        super(preferencesHelper);
        mAccountManager = accountManagerHelper;
        mUserRepository = userRepository;
    }

    @Override
    public Intent addAccountManager(User user, String password) {
        return mAccountManager.addAccount(user, password);
    }

    @Override
    public Observable<Boolean> saveUser(User user) {
        return mUserRepository.saveUser(user);
    }

}
