package com.ycjg.dietapp.ui.base;

import android.support.annotation.StringRes;
import android.widget.LinearLayout;

/**
 * Created by yragalvez on 12/11/2018.
 * DietApp
 */
public interface MvpView {

    void showLoading(LinearLayout rootView);

    void hideLoading();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(@StringRes int resId);

    void showMessage(String message);

    boolean isNetworkConnected();

    void hideKeyboard();
}
