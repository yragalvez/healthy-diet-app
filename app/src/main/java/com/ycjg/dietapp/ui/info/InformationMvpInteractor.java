package com.ycjg.dietapp.ui.info;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.ui.base.MvpInteractor;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 22/11/2018.
 * DietApp
 */
public interface InformationMvpInteractor extends MvpInteractor {
    Observable<User> getUser();

    Observable<Boolean> updateUser(User user, boolean isImperial);

    void setFirstLoginToFalse();

    boolean isFirstLogin();

    int getUserPlan();

    String getUserHeight();

    String getUserWeight();

    boolean isImperial();
}
