package com.ycjg.dietapp.ui.register;

import com.ycjg.dietapp.R;
import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.ui.base.BasePresenter;
import com.ycjg.dietapp.utils.AppLogger;
import com.ycjg.dietapp.utils.Utils;
import com.ycjg.dietapp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yragalvez on 15/11/2018.
 * DietApp
 */
public class RegistrationPresenter<V extends RegistrationMvpView, I extends RegistrationMvpInteractor>
        extends BasePresenter<V,I> implements RegistrationMvpPresenter<V, I> {

    private static final String TAG = RegistrationPresenter.class.getSimpleName();

    @Inject
    public RegistrationPresenter(I mvpInteractor,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void registerUser(String userName, String password, String retypedPassword, String contactNumber, String email) {
        boolean isValid = validateFields(userName, password, retypedPassword, contactNumber, email);
        if (isValid)
            register(userName, password, retypedPassword, contactNumber, email);
    }

    private boolean validateFields(String userName, String password, String retypedPassword, String contactNumber, String email) {
        boolean isValid = true;

        getMvpView().clearErrors();

        if (Utils.isTextInvalid(userName)) {
            isValid = false;
            getMvpView().setUserNameError(R.string.error_field_required);
        }

        if (Utils.isTextInvalid(password)) {
            isValid = false;
            if (password.length() <= 4)
                getMvpView().setPasswordError(R.string.error_invalid_password);
            else
                getMvpView().setPasswordError(R.string.error_field_required);
        }

        if (Utils.isTextInvalid(retypedPassword)) {
            isValid = false;
            getMvpView().setRetypedPasswordError(R.string.error_field_required);
        } else if (!retypedPassword.equals(password)) {
            isValid = false;
            getMvpView().setPasswordError(R.string.password_not_match);
            getMvpView().setRetypedPasswordError(R.string.password_not_match);
        }

        if (Utils.isTextInvalid(contactNumber)) {
            isValid = false;
            getMvpView().setContactNumberError(R.string.error_field_required);
        } else if (!Utils.isValidContactNumber(contactNumber)) {
            isValid = false;
            getMvpView().setContactNumberError(R.string.invalid_phone_number);
        }

        if (Utils.isTextInvalid(email)) {
            isValid = false;
            getMvpView().setEmailError(R.string.error_field_required);
        } else if (!Utils.isValidEmail(email)) {
            isValid = false;
            getMvpView().setEmailError(R.string.error_invalid_email);
        }

        return isValid;
    }

    private void register(String userName, String password, String retypedPassword, String contactNumber, String email) {
        Observable.fromCallable(User::new)
        .subscribeOn(getSchedulerProvider().io())
        .observeOn(getSchedulerProvider().ui())
        .subscribe(user -> {
            getMvpView().showMessage("Successfully registered user");
        }, throwable -> AppLogger.d(TAG, "register error()", throwable));
    }
}
