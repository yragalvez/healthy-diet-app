package com.ycjg.dietapp.ui.base;

/**
 * Created by yragalvez on 12/11/2018.
 * DietApp
 */
public interface DialogMvpView extends MvpView {

    void dismissDialog(String tag);

    void hideKeyboard();
}
