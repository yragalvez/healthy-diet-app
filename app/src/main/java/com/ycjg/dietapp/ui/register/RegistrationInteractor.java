package com.ycjg.dietapp.ui.register;

import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.ui.base.BaseInteractor;

import javax.inject.Inject;

/**
 * Created by yragalvez on 15/11/2018.
 * DietApp
 */
public class RegistrationInteractor extends BaseInteractor
        implements RegistrationMvpInteractor {

    @Inject
    public RegistrationInteractor(PreferencesHelper preferencesHelper) {
        super(preferencesHelper);
    }
}
