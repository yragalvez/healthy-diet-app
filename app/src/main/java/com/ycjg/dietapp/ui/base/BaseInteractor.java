package com.ycjg.dietapp.ui.base;

import com.ycjg.dietapp.data.prefs.PreferencesHelper;

import javax.inject.Inject;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public class BaseInteractor implements MvpInteractor {

    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public BaseInteractor(PreferencesHelper preferencesHelper) {
        mPreferencesHelper = preferencesHelper;
    }

    @Override
    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    @Override
    public void updateUserInfo(String userName, String email, String profilePicUrl) {
        getPreferencesHelper().setFirstLogin(email, true);
        getPreferencesHelper().setCurrentUserName(userName);
        getPreferencesHelper().setCurrentUserEmail(email);
        getPreferencesHelper().setCurrentUserProfilePicUrl(profilePicUrl);
    }
}
