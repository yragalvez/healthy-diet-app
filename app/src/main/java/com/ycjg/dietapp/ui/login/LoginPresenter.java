package com.ycjg.dietapp.ui.login;

import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.ycjg.dietapp.R;
import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.ui.base.BasePresenter;
import com.ycjg.dietapp.utils.AppLogger;
import com.ycjg.dietapp.utils.Utils;
import com.ycjg.dietapp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public class LoginPresenter<V extends LoginMvpView, I extends LoginMvpInteractor>
        extends BasePresenter<V, I> implements LoginMvpPresenter<V, I> {

    private static final String TAG = LoginPresenter.class.getSimpleName();

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    @Inject
    public LoginPresenter(I mvpInteractor,
                          SchedulerProvider schedulerProvider,
                          CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onServerLoginClick(String email, String password) {
        if (mAuthTask != null)
            return;

        if (!isViewAttached()) return;

        getMvpView().setEmailError(null);
        getMvpView().setPasswordError(null);

        boolean cancel = false;

        if (TextUtils.isEmpty(password)) {
            getMvpView().setPasswordError(R.string.error_field_required);
            cancel = true;
        } else if (!isPasswordValid(password)) {
            getMvpView().setPasswordError(R.string.error_invalid_password);
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            getMvpView().setEmailError(R.string.error_field_required);
            cancel = true;
        } else if (!Utils.isValidEmail(email)) {
            getMvpView().setEmailError(R.string.error_invalid_email);
            cancel = true;
        }

        if (!cancel) {
            getMvpView().showLoading();
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 4;
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                return false;
            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail))
                    return pieces[1].equals(mPassword);
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            mAuthTask = null;
            if (!isViewAttached()) return;
            getMvpView().hideLoading();
            if (success) {
                User user = new User();
                user.setEmail(mEmail);
                addToLocalRepo(user);
                getMvpView().onSuccessfulLogin(user, mPassword);
            }
        }

        @Override
        protected void onCancelled(Boolean aBoolean) {
            mAuthTask = null;
            if (!isViewAttached()) return;
            getMvpView().hideLoading();
        }
    }

    private void addToLocalRepo(User user) {
        getCompositeDisposable().add(getInteractor()
        .saveUser(user)
        .subscribeOn(getSchedulerProvider().io())
        .observeOn(getSchedulerProvider().ui())
        .subscribe(isInserted ->
                getInteractor()
                        .updateUserInfo(user.getFirstName() + user.getLastName(),
                                user.getEmail(), "")
                , throwable -> AppLogger.e(TAG, "addToLocalRepo()", throwable)));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public Intent addToAccountManager(User user, String password) {
        return getInteractor().addAccountManager(user, password);
    }
}
