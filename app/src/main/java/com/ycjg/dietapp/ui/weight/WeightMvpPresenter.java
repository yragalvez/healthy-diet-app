package com.ycjg.dietapp.ui.weight;

import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.ui.base.MvpPresenter;

/**
 * Created by yragalvez on 20/11/2018.
 * DietApp
 */
@PerActivity
public interface WeightMvpPresenter<V extends WeightMvpView,
        I extends WeightMvpInteractor> extends MvpPresenter<V, I> {
}
