package com.ycjg.dietapp.ui.main;

import com.ycjg.dietapp.ui.base.MvpView;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public interface MainMvpView extends MvpView {
    void showMenus();

    void showComputeBmi();
}
