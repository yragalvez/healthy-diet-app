package com.ycjg.dietapp.ui.login;

import android.support.annotation.StringRes;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.ui.base.MvpView;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public interface LoginMvpView extends MvpView {

    void setEmailError(String error);

    void setEmailError(@StringRes int resId);

    void setPasswordError(String error);

    void setPasswordError(@StringRes int resId);

    void openMainActivity();

    void onSuccessfulLogin(User user, String password);

    void showLoading();
}
