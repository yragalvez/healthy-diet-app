package com.ycjg.dietapp.ui.weight;

import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.ui.base.BaseInteractor;

import javax.inject.Inject;

/**
 * Created by yragalvez on 20/11/2018.
 * DietApp
 */
public class WeightInteractor extends BaseInteractor implements WeightMvpInteractor {

    @Inject
    public WeightInteractor(PreferencesHelper preferencesHelper) {
        super(preferencesHelper);
    }
}
