package com.ycjg.dietapp.ui.info;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anychart.AnyChartView;
import com.anychart.charts.LinearGauge;
import com.ycjg.dietapp.R;
import com.ycjg.dietapp.ui.base.BaseActivity;
import com.ycjg.dietapp.ui.diet.DietActivity;
import com.ycjg.dietapp.ui.views.LinearGaugeLayout;

import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created on 22/11/2018.
 * DietApp
 */
public class InformationActivity extends BaseActivity implements InformationMvpView {

    @Inject
    InformationMvpPresenter<InformationMvpView, InformationMvpInteractor> mPresenter;

    public static final int TEXT_HEIGHT_ERROR = 1;
    public static final int TEXT_WEIGHT_ERROR = 2;

    @BindView(R.id.progress_bar)
    protected ProgressBar mProgressBar;

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @BindView(R.id.anyChart)
    protected AnyChartView mAnyChartView;

    @BindView(R.id.rgGender)
    protected RadioGroup rgGender;

    @BindView(R.id.etAge)
    protected EditText etAge;

    @BindView(R.id.rgUnit)
    protected RadioGroup rgUnit;

    @BindView(R.id.tiHeight)
    protected TextInputLayout tiHeight;

    @BindView(R.id.etHeight)
    protected EditText etHeight;

    @BindView(R.id.tiWeight)
    protected TextInputLayout tiWeight;

    @BindView(R.id.etWeight)
    protected EditText etWeight;

    @BindView(R.id.btnCompute)
    protected Button btnCompute;

    @BindView(R.id.tvBmiLabel)
    protected TextView tvBmiLabel;

    @BindView(R.id.tvBmiValue)
    protected TextView tvBmiValue;

    @BindView(R.id.llComputeBmi)
    protected LinearLayout llComputeBmi;

    @BindView(R.id.rlBmi)
    protected RelativeLayout rlBmi;

    @BindView(R.id.btnPlan)
    protected Button btnPlan;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, InformationActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    protected void setUp() {
        setupToolbar();
        toggleTextUnit();
        addListener();
    }

    private void addListener() {
        rgUnit.setOnCheckedChangeListener((radioGroup, i) -> toggleTextUnit());
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mToolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.setupShownScreen();
    }

    @OnClick(R.id.btnCompute)
    void onComputeBmi() {
        mPresenter.computeBMI(etWeight.getText().toString().trim(),
                etHeight.getText().toString().trim(), isImperial());
    }

    @OnClick(R.id.btnPlan)
    void onChoosePlan() {
        mPresenter.setBmiButtonAction();
    }

    @Override
    public void choosePlan() {
        Intent intent = DietActivity.getStartIntent(this);
        startActivity(intent);
        finish();
    }

    private void toggleTextUnit() {
        String sHeight = getResources().getString(R.string.height);
        String sWeight = getResources().getString(R.string.weight);

        if (isImperial()) {
            sHeight += " (in)";
            sWeight += " (lb)";
        } else {
            sHeight += " (cm)";
            sWeight += " (kg)";
        }

        tiHeight.setHint(sHeight);
        tiWeight.setHint(sWeight);
    }

    private boolean isImperial() {
        return rgUnit.getCheckedRadioButtonId() == R.id.rbImperial;
    }

    @Override
    public void setupBmiValue(double bmiValue) {
        llComputeBmi.setVisibility(View.GONE);
        rlBmi.setVisibility(View.VISIBLE);

        tvBmiValue.setText(String.format(Locale.getDefault(), "%.2f", bmiValue));

        setupChart(bmiValue);
    }

    @Override
    public void showBmiComputation(boolean showInput) {
        llComputeBmi.setVisibility(showInput? View.VISIBLE : View.GONE);
        rlBmi.setVisibility(showInput? View.GONE : View.VISIBLE);
    }

    @Override
    public void setPlanButtonText(boolean choosePlan) {
        btnPlan.setText(choosePlan? getResources().getString(R.string.choose_your_plan) : getResources().getString(R.string.updte_bmi));
    }

    private void setupChart(double bmiValue) {
        rlBmi.setVisibility(View.VISIBLE);
        mAnyChartView.setProgressBar(mProgressBar);

        LinearGauge linearGauge = LinearGaugeLayout.getLinearGauge(bmiValue);
        mAnyChartView.setChart(linearGauge);
    }

    @Override
    public void setFieldRequiredError(int code) {
        switch (code) {
            case TEXT_HEIGHT_ERROR:
                etHeight.setError(getResources().getString(R.string.error_field_required));
                break;
            case TEXT_WEIGHT_ERROR:
                etWeight.setError(getResources().getString(R.string.error_field_required));
                break;
        }
    }

    @Override
    public void clearFieldErrors() {
        etHeight.setError(null);
        etWeight.setError(null);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
