package com.ycjg.dietapp.ui.info;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.data.db.repository.UserRepository;
import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.ui.base.BaseInteractor;
import com.ycjg.dietapp.utils.AppConstants;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 22/11/2018.
 * DietApp
 */
public class InformationInteractor extends BaseInteractor implements InformationMvpInteractor {

    private UserRepository mUserRepo;

    @Inject
    public InformationInteractor(PreferencesHelper preferencesHelper,
                                 UserRepository userRepository) {
        super(preferencesHelper);
        mUserRepo = userRepository;
    }

    @Override
    public Observable<User> getUser() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return mUserRepo.getUser(email);
        return null;
    }

    @Override
    public Observable<Boolean> updateUser(User user, boolean isImperial) {
        if (user.getEmail() != null) {
            getPreferencesHelper().setUserWeight(user.getEmail(), user.getWeight());
            getPreferencesHelper().setUserHeight(user.getEmail(), user.getHeight());
            getPreferencesHelper().setIsImperial(user.getEmail(), isImperial);
        }

        return mUserRepo.updateUser(user);
    }

    @Override
    public void setFirstLoginToFalse() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            if (getPreferencesHelper().isFirstLogin(email))
                getPreferencesHelper().setFirstLogin(email, false);
    }

    @Override
    public boolean isFirstLogin() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return getPreferencesHelper().isFirstLogin(email);
        return false;
    }

    @Override
    public int getUserPlan() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return getPreferencesHelper().getUserPlan(email);
        return AppConstants.DEFAULT_INT;
    }

    @Override
    public String getUserHeight() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return getPreferencesHelper().getUserHeight(email);
        return AppConstants.DEFAULT_STRING;
    }

    @Override
    public String getUserWeight() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return getPreferencesHelper().getUserWeight(email);
        return AppConstants.DEFAULT_STRING;
    }

    @Override
    public boolean isImperial() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return getPreferencesHelper().getIsImperial(email);
        return AppConstants.DEFAULT_STATUS;
    }
}
