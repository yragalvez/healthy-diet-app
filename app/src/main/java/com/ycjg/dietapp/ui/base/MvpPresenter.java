package com.ycjg.dietapp.ui.base;

/**
 * Created by yragalvez on 12/11/2018.
 * DietApp
 */
public interface MvpPresenter<V extends MvpView, I extends MvpInteractor> {

    void onAttach(V mvpView);

    void onDetach();

    V getMvpView();

    I getInteractor();

    boolean isViewAttached();

    void checkViewAttached() throws BasePresenter.MvpViewNotAttachedException;
}
