package com.ycjg.dietapp.ui.weight.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ycjg.dietapp.ui.base.BaseViewHolder;

/**
 * Created by yragalvez on 21/11/2018.
 * DietApp
 */
public class WeightAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public WeightAdapter() {

    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
//        View rootView = inflater.inflate(R.layout.layout_payment_item, viewGroup, false);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(baseViewHolder.getAdapterPosition());
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
