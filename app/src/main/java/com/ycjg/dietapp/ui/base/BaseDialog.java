package com.ycjg.dietapp.ui.base;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ycjg.dietapp.di.component.ActivityComponent;

import butterknife.Unbinder;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public abstract class BaseDialog extends DialogFragment implements DialogMvpView {

    private BaseActivity mActivity;
    private Unbinder mUnbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity mActivity = (BaseActivity) context;
            this.mActivity = mActivity;
            mActivity.onFragmentAttached();
        }
    }

    @Override
    public void showLoading(LinearLayout rootView) {
        if (mActivity != null)
            mActivity.showLoading(rootView);
    }

    @Override
    public void hideLoading() {
        if (mActivity != null)
            mActivity.hideLoading();
    }

    @Override
    public void hideKeyboard() {
        if (mActivity != null)
            mActivity.hideKeyboard();
    }

    @Override
    public void onError(@StringRes int resId) {
        if (mActivity != null)
            mActivity.onError(resId);
    }

    @Override
    public void onError(String message) {
        if (mActivity != null)
            mActivity.onError(message);
    }

    @Override
    public void showMessage(@StringRes int resId) {
        if (mActivity != null)
            mActivity.showMessage(resId);
    }

    @Override
    public void showMessage(String message) {
        if (mActivity != null)
            mActivity.showMessage(message);
    }

    @Override
    public boolean isNetworkConnected() {
        if (mActivity != null)
            return mActivity.isNetworkConnected();
        return false;
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public ActivityComponent getActivityComponent() {
        if (mActivity != null)
            return mActivity.getActivityComponent();
        return null;
    }

    public void setUnbinder(Unbinder unbinder) {
        mUnbinder = unbinder;
    }

    protected abstract void setUp(View view);

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUp(view);
    }

    public void show(FragmentManager fragmentManager, String tag) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment prevFragment = fragmentManager.findFragmentByTag(tag);
        if (prevFragment != null)
            transaction.remove(prevFragment);
        transaction.addToBackStack(null);
        show(transaction, tag);
    }

    @Override
    public void dismissDialog(String tag) {
        dismissAllowingStateLoss();
        getBaseActivity().onFragmentDetached(tag);
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null)
            mUnbinder.unbind();
        super.onDestroy();
    }
}
