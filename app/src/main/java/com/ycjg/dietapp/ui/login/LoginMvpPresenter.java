package com.ycjg.dietapp.ui.login;

import android.content.Intent;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.ui.base.MvpPresenter;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
@PerActivity
public interface LoginMvpPresenter<V extends LoginMvpView,
        I extends LoginMvpInteractor> extends MvpPresenter<V, I> {

    void onServerLoginClick(String email, String password);

    Intent addToAccountManager(User user, String password);
}
