package com.ycjg.dietapp.ui.diet;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.data.db.WeightPlan;
import com.ycjg.dietapp.ui.base.MvpInteractor;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 14/11/2018.
 * DietApp
 */
public interface DietMvpInteractor extends MvpInteractor {
    Observable<User> getUser();

    int getUserPlan();

    void setUserPlan(@WeightPlan.Weight int plan);
}
