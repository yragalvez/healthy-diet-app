package com.ycjg.dietapp.ui.weight;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ycjg.dietapp.R;
import com.ycjg.dietapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeightActivity extends BaseActivity implements WeightMvpView {

    @Inject
    WeightMvpPresenter<WeightMvpView, WeightMvpInteractor> mPresenter;

    @BindView(R.id.tvWeightPlan)
    protected TextView tvWeightPlan;

    @BindView(R.id.rvWeightPlanMenu)
    protected RecyclerView rvWeightPlanMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);

        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    protected void setUp() {

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
