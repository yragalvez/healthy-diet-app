package com.ycjg.dietapp.ui.login;

import android.content.Intent;

import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.ui.base.MvpInteractor;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public interface LoginMvpInteractor extends MvpInteractor {
    Intent addAccountManager(User user, String password);

    Observable<Boolean> saveUser(User user);
}
