package com.ycjg.dietapp.ui.diet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ycjg.dietapp.R;
import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.data.db.WeightPlan;
import com.ycjg.dietapp.ui.base.BaseActivity;
import com.ycjg.dietapp.ui.info.InformationActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ycjg.dietapp.data.db.WeightPlan.Weight;

public class DietActivity extends BaseActivity
        implements DietMvpView, NavigationView.OnNavigationItemSelectedListener {

    @Inject
    DietMvpPresenter<DietMvpView, DietMvpInteractor> mPresenter;

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @BindView(R.id.drawer_layout)
    protected DrawerLayout mDrawer;

    @BindView(R.id.nav_view)
    protected NavigationView mNavigation;

    @BindView(R.id.llUserProgress)
    protected LinearLayout llUserProgress;

    @BindView(R.id.llDietActivity)
    protected LinearLayout llDietActivity;

    protected ImageView ivProfilePicture;
    protected TextView tvUserName;
    protected TextView tvEmail;

    @BindView(R.id.cvProgress)
    protected CardView cvProgress;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, DietActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet);

        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    protected void setUp() {
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigation.setNavigationItemSelectedListener(this);
        setupNavMenu();
    }

    private void setupNavMenu() {
        View headerLayout = mNavigation.getHeaderView(0);
        ivProfilePicture = headerLayout.findViewById(R.id.ivProfilePicture);
        tvUserName = headerLayout.findViewById(R.id.tvUserName);
        tvEmail = headerLayout.findViewById(R.id.tvEmail);
        mPresenter.getUser();
    }

    @Override
    public void setUserDataHeader(User user) {
        if (user == null) return;
        tvUserName.setText(user.getUserId());
        tvEmail.setText(user.getEmail());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.setupLayout();
    }

    @Override
    public void showUserProgressLayout(boolean hasPlan) {
        llUserProgress.setVisibility(hasPlan? View.VISIBLE : View.GONE);
        llDietActivity.setVisibility(hasPlan? View.GONE : View.VISIBLE);
    }

    @OnClick(R.id.cvLoseWeight)
    void onSelectedLoseWeight() {
        createProceedDialog(WeightPlan.LOSE_WEIGHT);
    }

    @OnClick(R.id.cvMaintainWeight)
    void onSelectedMaintainWeight() {
        createProceedDialog(WeightPlan.MAINTAIN_WEIGHT);
    }

    @OnClick(R.id.cvGainWeight)
    void onSelectedGainWeight() {
        createProceedDialog(WeightPlan.GAIN_WEIGHT);
    }

    private void createProceedDialog(@Weight int plan) {
        String planType = WeightPlan.getTitle(plan);
        AlertDialog.Builder builder = new AlertDialog.Builder(DietActivity.this);
        builder.setMessage(String.format("You are about to select %s. Do you want to continue?", planType));
        builder.setPositiveButton("Proceed", ((dialogInterface, i) -> {}));
        builder.setNegativeButton("Cancel", ((dialogInterface, i) -> dialogInterface.cancel()));
        builder.setCancelable(false);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button positiveBtn = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveBtn.setTextColor(getResources().getColor(R.color.black));
        Button negativeBtn = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);

        positiveBtn.setOnClickListener(view -> {
            positiveBtn.setEnabled(false);
            negativeBtn.setEnabled(false);
            alertDialog.dismiss();

            mPresenter.setUserSelectedPlan(plan);
        });
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.diet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_progress:
                break;
            case R.id.nav_bmi:
                navigateToBmi();
                break;
            case R.id.nav_logout:
                mPresenter.logout();
                break;
            default:
                break;
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void navigateToBmi() {
        Intent intent = InformationActivity.getStartIntent(this);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
