package com.ycjg.dietapp.ui.register;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.ycjg.dietapp.R;
import com.ycjg.dietapp.ui.base.BaseActivity;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends BaseActivity implements RegistrationMvpView {

    @Inject
    RegistrationMvpPresenter<RegistrationMvpView, RegistrationMvpInteractor> mPresenter;

    @BindView(R.id.llProfilePicture)
    protected View llProfilePicture;

    @BindView(R.id.ivProfilePicture)
    protected ImageView ivProfilePicture;

    @BindView(R.id.etUserName)
    protected EditText etUserName;

    @BindView(R.id.etPassword)
    protected EditText etPassword;

    @BindView(R.id.etConfirmPassword)
    protected EditText etConfirmPassword;

    @BindView(R.id.etContactNumber)
    protected EditText etContactNumber;

    @BindView(R.id.etEmail)
    protected EditText etEmail;

    @BindView(R.id.btnRegister)
    protected Button btnRegister;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, RegistrationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        getActivityComponent().inject(this);
        setUnbinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);

        setUp();
    }

    @Override
    protected void setUp() {
        initializeToolbar();
    }

    private void initializeToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @OnClick(R.id.btnRegister)
    void onRegisterUser() {
        registerUser();
    }

    private void registerUser() {
        mPresenter.registerUser(etUserName.getText().toString(), etPassword.getText().toString(), etConfirmPassword.getText().toString(),
                etContactNumber.getText().toString(), etEmail.getText().toString());
    }

    @Override
    public void setUserNameError(@StringRes int id) {
        etUserName.setError(getResources().getString(id));
    }

    @Override
    public void setUserNameError(String message) {
        etUserName.setError(message);
    }

    @Override
    public void setPasswordError(@StringRes int id) {
        etPassword.setError(getResources().getString(id));
    }

    @Override
    public void setPasswordError(String message) {
        etPassword.setError(message);
    }

    @Override
    public void setRetypedPasswordError(@StringRes int id) {
        etConfirmPassword.setError(getResources().getString(id));
    }

    @Override
    public void setRetypedPasswordError(String message) {
        etConfirmPassword.setError(message);
    }

    @Override
    public void setContactNumberError(@StringRes int id) {
        etContactNumber.setError(getResources().getString(id));
    }

    @Override
    public void setContactNumberError(String message) {
        etContactNumber.setError(message);
    }

    @Override
    public void setEmailError(@StringRes int id) {
        etEmail.setError(getResources().getString(id));
    }

    @Override
    public void setEmailError(String message) {
        etEmail.setError(message);
    }

    @Override
    public void clearErrors() {
        setUserNameError(null);
        setPasswordError(null);
        setRetypedPasswordError(null);
        setContactNumberError(null);
        setEmailError(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }
}
