package com.ycjg.dietapp.ui.main;

import com.ycjg.dietapp.ui.base.MvpInteractor;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public interface MainMvpInteractor extends MvpInteractor {

    boolean isFirstLogin();
}
