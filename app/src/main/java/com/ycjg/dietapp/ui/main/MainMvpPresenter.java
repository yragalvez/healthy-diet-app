package com.ycjg.dietapp.ui.main;

import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.ui.base.MvpPresenter;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
@PerActivity
public interface MainMvpPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends MvpPresenter<V, I> {

    void authenticateActivity();

    void setupShownScreen();
}
