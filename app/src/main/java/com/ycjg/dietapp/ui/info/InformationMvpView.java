package com.ycjg.dietapp.ui.info;

import com.ycjg.dietapp.ui.base.MvpView;

/**
 * Created by yragalvez on 22/11/2018.
 * DietApp
 */
public interface InformationMvpView extends MvpView {
    void choosePlan();

    void setupBmiValue(double bmiValue);

    void showBmiComputation(boolean showInput);

    void setPlanButtonText(boolean choosePlan);

    void setFieldRequiredError(int code);

    void clearFieldErrors();
}
