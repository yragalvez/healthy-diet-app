package com.ycjg.dietapp.ui.main;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import com.ycjg.dietapp.data.account_manager.AccountManagerHelper;
import com.ycjg.dietapp.ui.base.BasePresenter;
import com.ycjg.dietapp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created on 13/11/2018.
 * DietApp
 */
public class MainPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends BasePresenter<V,I> implements MainMvpPresenter<V, I> {

    private final AccountManagerHelper mManager;
    private Activity mActivity;

    @Inject
    public MainPresenter(I mvpInteractor,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable,
                         AccountManagerHelper accountManagerHelper,
                         AppCompatActivity activity) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
        mManager = accountManagerHelper;
        mActivity = activity;
    }

    @Override
    public void authenticateActivity() {
        mManager.authenticateActivity(mActivity);
    }

    @Override
    public void setupShownScreen() {
        if (!isViewAttached()) return;
        if (mManager.isLoggedId())
            if (getInteractor().isFirstLogin())
                getMvpView().showComputeBmi();
            else getMvpView().showMenus();
    }
}
