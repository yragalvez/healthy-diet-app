package com.ycjg.dietapp.ui.main;

import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.ui.base.BaseInteractor;

import javax.inject.Inject;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
public class MainInteractor extends BaseInteractor
        implements MainMvpInteractor {

    @Inject
    public MainInteractor(PreferencesHelper preferencesHelper) {
        super(preferencesHelper);
    }

    @Override
    public boolean isFirstLogin() {
        String email = getPreferencesHelper().getCurrentUserEmail();
        if (email != null)
            return getPreferencesHelper().isFirstLogin(email);
        return false;
    }
}
