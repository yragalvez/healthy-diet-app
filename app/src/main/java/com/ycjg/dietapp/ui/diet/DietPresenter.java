package com.ycjg.dietapp.ui.diet;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import com.ycjg.dietapp.data.account_manager.AccountManagerHelper;
import com.ycjg.dietapp.data.db.WeightPlan.Weight;
import com.ycjg.dietapp.ui.base.BasePresenter;
import com.ycjg.dietapp.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yragalvez on 14/11/2018.
 * DietApp
 */
public class DietPresenter<V extends DietMvpView, I extends DietMvpInteractor>
        extends BasePresenter<V, I> implements DietMvpPresenter<V, I> {

    private AccountManagerHelper mManagerHelper;
    private final Activity mActivity;

    @Inject
    public DietPresenter(I mvpInteractor,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable,
                         AccountManagerHelper accountManagerHelper,
                         AppCompatActivity activity) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
        mManagerHelper = accountManagerHelper;
        mActivity = activity;
    }

    @Override
    public void getUser() {
        if (!isViewAttached()) return;
        getMvpView().setUserDataHeader(mManagerHelper.getUserData());
    }

    @Override
    public void logout() {
        mManagerHelper.logout(mActivity);
    }

    @Override
    public void setupLayout() {
        if (!isViewAttached()) return;

        getMvpView().showUserProgressLayout(getInteractor().getUserPlan() > 0);
    }

    @Override
    public void setUserSelectedPlan(@Weight int plan) {
        getInteractor().setUserPlan(plan);
        setupLayout();
    }
}
