package com.ycjg.dietapp.ui.register;

import android.support.annotation.StringRes;

import com.ycjg.dietapp.ui.base.MvpView;

/**
 * Created by yragalvez on 15/11/2018.
 * DietApp
 */
public interface RegistrationMvpView extends MvpView {
    void setUserNameError(@StringRes int id);

    void setUserNameError(String message);

    void setPasswordError(@StringRes int id);

    void setPasswordError(String message);

    void setRetypedPasswordError(@StringRes int id);

    void setRetypedPasswordError(String message);

    void setContactNumberError(@StringRes int id);

    void setContactNumberError(String message);

    void setEmailError(@StringRes int id);

    void setEmailError(String message);

    void clearErrors();
}
