package com.ycjg.dietapp.ui.views;

import android.support.annotation.NonNull;

import com.anychart.AnyChart;
import com.anychart.chart.common.dataentry.SingleValueDataSet;
import com.anychart.charts.LinearGauge;
import com.anychart.enums.Anchor;
import com.anychart.enums.Layout;
import com.anychart.enums.MarkerType;
import com.anychart.enums.Orientation;
import com.anychart.enums.Position;
import com.anychart.scales.OrdinalColor;

/**
 * Created by yragalvez on 26/11/2018.
 * DietApp
 */
public class LinearGaugeLayout {

    @NonNull
    public static LinearGauge getLinearGauge(double value) {
        LinearGauge linearGauge = AnyChart.linear();
        linearGauge.data(new SingleValueDataSet(new Double[] { value }));

        linearGauge.layout(Layout.HORIZONTAL);

        linearGauge.label(0)
                .position(Position.LEFT_CENTER)
                .anchor(Anchor.LEFT_CENTER)
                .offsetY("-50px")
                .offsetX("30px")
                .fontColor("black")
                .fontSize(17);
        linearGauge.label(0).text("Your BMI Category");

        linearGauge.label(1)
                .position(Position.LEFT_CENTER)
                .anchor(Anchor.LEFT_CENTER)
                .offsetY("40px")
                .offsetX("20px")
                .fontColor("#777777")
                .fontSize(12);
        linearGauge.label(1).text("Underweight");

        linearGauge.label(2)
                .position(Position.CENTER)
                .anchor(Anchor.CENTER)
                .offsetY("40px")
                .offsetX("0px")
                .fontColor("#777777")
                .fontSize(12);
        linearGauge.label(2).text("Normal Weight");

        linearGauge.label(3)
                .position(Position.RIGHT_CENTER)
                .anchor(Anchor.RIGHT_CENTER)
                .offsetY("40px")
                .offsetX("100px")
                .fontColor("#777777")
                .fontSize(12);
        linearGauge.label(3).text("Overweight");

        linearGauge.label(4)
                .position(Position.RIGHT_CENTER)
                .anchor(Anchor.RIGHT_CENTER)
                .offsetY("40px")
                .offsetX("50px")
                .fontColor("#777777")
                .fontSize(12);
        linearGauge.label(4).text("Obese");

        OrdinalColor scaleBarColorScale = OrdinalColor.instantiate();
        scaleBarColorScale.ranges(new String[]{
                "{ from: 0, to: 18.5, color: ['yellow 1'] }",
                "{ from: 18.5, to: 25, color: ['green 1'] }",
                "{ from: 25, to: 30, color: ['orange 1'] }",
                "{ from: 30, to: 40, color: ['red 1'] }"
        });

        linearGauge.scaleBar(0)
                .width("5%")
                .colorScale(scaleBarColorScale);

        linearGauge.marker(0)
                .type(MarkerType.TRIANGLE_UP)
                .color("red")
                .offset("5%")
                .zIndex(40);

        linearGauge.scale()
                .minimum(0)
                .maximum(40);

        linearGauge.axis(0)
                .minorTicks(false)
                .width("1%");

        linearGauge.axis(0)
                .offset("-1.5%")
                .orientation(Orientation.TOP)
                .labels("top");

        linearGauge.padding(0, 30, 0, 30);

        return linearGauge;
    }
}
