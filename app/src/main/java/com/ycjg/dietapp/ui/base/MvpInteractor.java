package com.ycjg.dietapp.ui.base;

import com.ycjg.dietapp.data.prefs.PreferencesHelper;

/**
 * Created by yragalvez on 12/11/2018.
 * DietApp
 */
public interface MvpInteractor {

    PreferencesHelper getPreferencesHelper();

    void updateUserInfo(String userName, String email, String profilePicUrl);
}
