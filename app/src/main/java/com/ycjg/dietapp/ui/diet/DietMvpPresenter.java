package com.ycjg.dietapp.ui.diet;

import com.ycjg.dietapp.data.db.WeightPlan;
import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.ui.base.MvpPresenter;

/**
 * Created by yragalvez on 14/11/2018.
 * DietApp
 */
@PerActivity
public interface DietMvpPresenter<V extends DietMvpView, I extends DietMvpInteractor>
        extends MvpPresenter<V, I> {
    void getUser();

    void logout();

    void setupLayout();

    void setUserSelectedPlan(@WeightPlan.Weight int plan);
}
