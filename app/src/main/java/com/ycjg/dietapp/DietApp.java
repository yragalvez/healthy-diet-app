package com.ycjg.dietapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.ycjg.dietapp.di.component.ApplicationComponent;
import com.ycjg.dietapp.di.component.DaggerApplicationComponent;
import com.ycjg.dietapp.di.module.ApplicationModule;
import com.ycjg.dietapp.utils.AppLogger;
import com.ycjg.dietapp.utils.NetworkUtils;

/**
 * Created by yragalvez on 08/11/2018.
 * DietApp
 */
public class DietApp extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();

        mApplicationComponent.inject(this);

        NetworkUtils.get().registerNetworkChangeReceiver(this);
        AppLogger.init();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
