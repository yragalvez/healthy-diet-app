package com.ycjg.dietapp.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by yragalvez on 11/11/2018.
 * DietApp
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
