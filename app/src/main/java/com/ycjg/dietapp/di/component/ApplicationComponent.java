package com.ycjg.dietapp.di.component;

import android.app.Application;
import android.content.Context;

import com.ycjg.dietapp.DietApp;
import com.ycjg.dietapp.data.account_manager.AccountManagerHelper;
import com.ycjg.dietapp.data.db.DaoSession;
import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.di.ApplicationContext;
import com.ycjg.dietapp.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by yragalvez on 11/11/2018.
 * DietApp
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(DietApp app);

    @ApplicationContext
    Context context();

    Application application();

    PreferencesHelper preferencesHelper();

    AccountManagerHelper accountManagerHelper();

    DaoSession daoSession();
}
