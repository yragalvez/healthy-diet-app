package com.ycjg.dietapp.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.ycjg.dietapp.di.ActivityContext;
import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.ui.diet.DietInteractor;
import com.ycjg.dietapp.ui.diet.DietMvpInteractor;
import com.ycjg.dietapp.ui.diet.DietMvpPresenter;
import com.ycjg.dietapp.ui.diet.DietMvpView;
import com.ycjg.dietapp.ui.diet.DietPresenter;
import com.ycjg.dietapp.ui.info.InformationInteractor;
import com.ycjg.dietapp.ui.info.InformationMvpInteractor;
import com.ycjg.dietapp.ui.info.InformationMvpPresenter;
import com.ycjg.dietapp.ui.info.InformationMvpView;
import com.ycjg.dietapp.ui.info.InformationPresenter;
import com.ycjg.dietapp.ui.login.LoginInteractor;
import com.ycjg.dietapp.ui.login.LoginMvpInteractor;
import com.ycjg.dietapp.ui.login.LoginMvpPresenter;
import com.ycjg.dietapp.ui.login.LoginMvpView;
import com.ycjg.dietapp.ui.login.LoginPresenter;
import com.ycjg.dietapp.ui.main.MainInteractor;
import com.ycjg.dietapp.ui.main.MainMvpInteractor;
import com.ycjg.dietapp.ui.main.MainMvpPresenter;
import com.ycjg.dietapp.ui.main.MainMvpView;
import com.ycjg.dietapp.ui.main.MainPresenter;
import com.ycjg.dietapp.ui.register.RegistrationInteractor;
import com.ycjg.dietapp.ui.register.RegistrationMvpInteractor;
import com.ycjg.dietapp.ui.register.RegistrationMvpPresenter;
import com.ycjg.dietapp.ui.register.RegistrationMvpView;
import com.ycjg.dietapp.ui.register.RegistrationPresenter;
import com.ycjg.dietapp.ui.weight.WeightInteractor;
import com.ycjg.dietapp.ui.weight.WeightMvpInteractor;
import com.ycjg.dietapp.ui.weight.WeightMvpPresenter;
import com.ycjg.dietapp.ui.weight.WeightMvpView;
import com.ycjg.dietapp.ui.weight.WeightPresenter;
import com.ycjg.dietapp.utils.rx.AppSchedulerProvider;
import com.ycjg.dietapp.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by yragalvez on 11/11/2018.
 * DietApp
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    /* Provide method for Presenters */

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView, LoginMvpInteractor> provideLoginPresenter(
            LoginPresenter<LoginMvpView, LoginMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView, MainMvpInteractor> provideMainPresenter(
            MainPresenter<MainMvpView, MainMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DietMvpPresenter<DietMvpView, DietMvpInteractor> provideDietPresenter(
            DietPresenter<DietMvpView, DietMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RegistrationMvpPresenter<RegistrationMvpView, RegistrationMvpInteractor> provideRegistrationPresenter(
            RegistrationPresenter<RegistrationMvpView, RegistrationMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    WeightMvpPresenter<WeightMvpView, WeightMvpInteractor> provideWeightPresenter(
            WeightPresenter<WeightMvpView, WeightMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    InformationMvpPresenter<InformationMvpView, InformationMvpInteractor> provideInformationPresenter(
            InformationPresenter<InformationMvpView, InformationMvpInteractor> presenter) {
        return presenter;
    }
    /* Provide method for Interactors */

    @Provides
    @PerActivity
    LoginMvpInteractor provideLoginMvpInteractor(LoginInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    MainMvpInteractor provideMainMvpInteractor(MainInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    DietMvpInteractor provideDietMvpInteractor(DietInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    RegistrationMvpInteractor provideRegistrationMvpInteractor(RegistrationInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    WeightMvpInteractor provideWeightMvpInteractor(WeightInteractor interactor) {
        return interactor;
    }

    @Provides
    @PerActivity
    InformationMvpInteractor provideMvpInteractor(InformationInteractor interactor) {
        return interactor;
    }
}
