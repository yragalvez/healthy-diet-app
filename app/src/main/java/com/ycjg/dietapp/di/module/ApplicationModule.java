package com.ycjg.dietapp.di.module;

import android.app.Application;
import android.content.Context;

import com.ycjg.dietapp.data.account_manager.AccountManagerHelper;
import com.ycjg.dietapp.data.account_manager.AppAccountManagerHelper;
import com.ycjg.dietapp.data.db.DaoMaster;
import com.ycjg.dietapp.data.db.DaoSession;
import com.ycjg.dietapp.data.db.DbOpenHelper;
import com.ycjg.dietapp.data.prefs.AppPreferencesHelper;
import com.ycjg.dietapp.data.prefs.PreferencesHelper;
import com.ycjg.dietapp.di.ApplicationContext;
import com.ycjg.dietapp.di.DatabaseInfo;
import com.ycjg.dietapp.di.PreferenceInfo;
import com.ycjg.dietapp.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by yragalvez on 11/11/2018.
 * DietApp
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    AccountManagerHelper provideAccountManagerHelper(AppAccountManagerHelper appAccountManagerHelper) {
        return appAccountManagerHelper;
    }

    @Provides
    @Singleton
    DaoSession provideDaoSession(DbOpenHelper dbOpenHelper) {
        return new DaoMaster(dbOpenHelper.getWritableDb()).newSession();
    }
}
