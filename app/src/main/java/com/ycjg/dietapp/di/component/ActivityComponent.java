package com.ycjg.dietapp.di.component;

import com.ycjg.dietapp.di.PerActivity;
import com.ycjg.dietapp.di.module.ActivityModule;
import com.ycjg.dietapp.ui.diet.DietActivity;
import com.ycjg.dietapp.ui.info.InformationActivity;
import com.ycjg.dietapp.ui.login.LoginActivity;
import com.ycjg.dietapp.ui.main.MainActivity;
import com.ycjg.dietapp.ui.register.RegistrationActivity;
import com.ycjg.dietapp.ui.weight.WeightActivity;

import dagger.Component;

/**
 * Created by yragalvez on 11/11/2018.
 * DietApp
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(LoginActivity activity);

    void inject(DietActivity activity);

    void inject(RegistrationActivity activity);

    void inject(WeightActivity activity);

    void inject(InformationActivity activity);
}
