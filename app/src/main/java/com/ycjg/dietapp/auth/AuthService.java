package com.ycjg.dietapp.auth;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by yragalvez on 08/11/2018.
 * DietApp
 */
public class AuthService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new Authenticator(this).getIBinder();
    }
}
