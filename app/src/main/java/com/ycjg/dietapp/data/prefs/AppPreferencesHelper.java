package com.ycjg.dietapp.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.ycjg.dietapp.di.ApplicationContext;
import com.ycjg.dietapp.di.PreferenceInfo;
import com.ycjg.dietapp.utils.AppConstants;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.ycjg.dietapp.data.db.WeightPlan.Weight;

/**
 * Created by yragalvez on 12/11/2018.
 * DietApp
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_FIRST_LOG_IN = "/pref_key_first_log_in";
    private static final String PREF_KEY_CURRENT_USERNAME = "/pref_key_current_username";
    private static final String PREF_KEY_CURRENT_EMAIL = "/pref_key_current_email";
    private static final String PREF_KEY_CURRENT_PROFILE_PIC_URL = "/pref_key_current_profile_pic_url";
    private static final String PREF_KEY_USER_WEIGHT = "/pref_key_user_weight";
    private static final String PREF_KEY_USER_HEIGHT = "/pref_key_user_height";
    private static final String PREF_KEY_IS_IMPERIAL = "/pref_key_is_imperial";
    private static final String PREF_KEY_USER_PLAN = "/pref_key_user_plan";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void setFirstLogin(String email, boolean isFirstLogin) {
        mPrefs.edit().putBoolean(getKeyPrefix(email, PREF_KEY_FIRST_LOG_IN), isFirstLogin).apply();
    }

    @Override
    public boolean isFirstLogin(String email) {
        return mPrefs.getBoolean(getKeyPrefix(email, PREF_KEY_FIRST_LOG_IN), AppConstants.DEFAULT_STATUS);
    }

    @Override
    public void setCurrentUserName(String username) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USERNAME, username).apply();
    }

    @Override
    public String getCurrentUserName() {
        return mPrefs.getString(PREF_KEY_CURRENT_USERNAME, AppConstants.DEFAULT_STRING);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_EMAIL, email).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPrefs.getString(PREF_KEY_CURRENT_EMAIL, AppConstants.DEFAULT_STRING);
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_PROFILE_PIC_URL, profilePicUrl).apply();
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPrefs.getString(PREF_KEY_CURRENT_PROFILE_PIC_URL, AppConstants.DEFAULT_STRING);
    }

    @Override
    public void setUserWeight(String email, double weight) {
        mPrefs.edit().putString(getKeyPrefix(email, PREF_KEY_USER_WEIGHT), String.valueOf(weight)).apply();
    }

    @Override
    public String getUserWeight(String email) {
        return mPrefs.getString(getKeyPrefix(email, PREF_KEY_USER_WEIGHT), AppConstants.DEFAULT_STRING);
    }

    @Override
    public void setUserHeight(String email, double height) {
        mPrefs.edit().putString(getKeyPrefix(email, PREF_KEY_USER_HEIGHT), String.valueOf(height)).apply();
    }

    @Override
    public String getUserHeight(String email) {
        return mPrefs.getString(getKeyPrefix(email, PREF_KEY_USER_HEIGHT), AppConstants.DEFAULT_STRING);
    }

    @Override
    public void setIsImperial(String email, boolean isImperial) {
        mPrefs.edit().putBoolean(getKeyPrefix(email, PREF_KEY_IS_IMPERIAL), isImperial).apply();
    }

    @Override
    public boolean getIsImperial(String email) {
        return mPrefs.getBoolean(getKeyPrefix(email, PREF_KEY_IS_IMPERIAL), AppConstants.DEFAULT_STATUS);
    }

    @Override
    public void setUserPlan(String email, @Weight int i) {
        mPrefs.edit().putInt(getKeyPrefix(email, PREF_KEY_USER_PLAN), i).apply();
    }

    @Override
    public int getUserPlan(String email) {
        return mPrefs.getInt(getKeyPrefix(email, PREF_KEY_USER_PLAN), AppConstants.DEFAULT_INT);
    }

    @NonNull
    private String getKeyPrefix(String email, String key) {
        return "email-" + email.trim() + key;
    }

}
