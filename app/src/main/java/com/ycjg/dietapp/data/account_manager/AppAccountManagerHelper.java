package com.ycjg.dietapp.data.account_manager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import com.ycjg.dietapp.R;
import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.di.ApplicationContext;
import com.ycjg.dietapp.ui.info.InformationActivity;
import com.ycjg.dietapp.ui.main.MainActivity;
import com.ycjg.dietapp.utils.AppConstants;
import com.ycjg.dietapp.utils.AppLogger;
import com.ycjg.dietapp.utils.DbHelper;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
@Singleton
public class AppAccountManagerHelper implements AccountManagerHelper {

    private static final String TAG = AppAccountManagerHelper.class.getSimpleName();

    private final AccountManager mAccountManager;
    private final Context mContext;
    private Account mAccount;
    private String accessToken = "A91oCKGTe+rM0rsY5UAqZPEc238T8hRCqpqmNVTJ74woYt8uiNQCJB0LLpMyfGZ5OME9cuyPcJQOhKUcgfB5y0iqjG9zjT7GAMW1250Mv1Cwm4iwUoHZAr4V6ZrCeRUP";

    @Inject
    public AppAccountManagerHelper(@ApplicationContext Context context) {
        mContext = context;
        mAccountManager = AccountManager.get(context);
        loadAndroidAccount();
    }

    private Account loadAndroidAccount() {
        Account[] availableAccounts = mAccountManager.getAccountsByType(AppConstants.ACCOUNT_TYPE);
        if (availableAccounts.length > 0)
           mAccount = availableAccounts[0];
        else mAccount = null;
        return mAccount;
    }

    @Override
    public boolean isLoggedId() {
        mAccount = loadAndroidAccount();
        return mAccount != null;
    }

    @Override
    public void authenticateActivity(final Activity activity) {
        if (!isLoggedId()) {
            login(activity);
            activity.finish();
        }
    }

    @Override
    public User getUserData() {
        loadAndroidAccount();
        if (mAccount != null) {
            String data = mAccountManager.getUserData(mAccount, AppConstants.USER_INFO_KEY);
            return User.parse(data);
        }
        return null;
    }

    private void login(final Activity activity) {
        if (mAccountManager != null)
            mAccountManager.addAccount(AppConstants.ACCOUNT_TYPE, AppConstants.GENERAL_KEY_TYPE,
                    null, null, activity, (future) -> {
                        try {
                            future.getResult();
                            activity.startActivity(InformationActivity.getStartIntent(activity));
                        } catch (AuthenticatorException | OperationCanceledException e) {
                            AppLogger.e(TAG, "login() AuthenticatorException | OperationCanceledException", e);
                            MainActivity.goToMain(activity, true);
                            activity.finish();
                        } catch (IOException e) {
                            AppLogger.e(TAG, "login()", e);
                        }
                    }, null);
    }

    @Override
    public Intent addAccount(User user, String password) {
        //TODO: get access token
        String email = user.getEmail();

        Account account = new Account(email, AppConstants.ACCOUNT_TYPE);
        mAccountManager.addAccountExplicitly(account, null, null);
        mAccountManager.setUserData(account, AppConstants.USER_INFO_KEY, DbHelper.toJson(user));
        mAccountManager.setAuthToken(account, AppConstants.GENERAL_KEY_TYPE, accessToken);

        Intent intent = new Intent();
        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, email);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, AppConstants.ACCOUNT_TYPE);
        intent.putExtra(AccountManager.KEY_AUTHTOKEN, accessToken);

        return intent;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    @Override
    public void logout(final Activity activity) {
        loadAndroidAccount();

        mAccountManager.setUserData(mAccount, AppConstants.USER_INFO_KEY, null);
        mAccountManager.invalidateAuthToken(AppConstants.ACCOUNT_TYPE, accessToken);
        mAccountManager.removeAccount(mAccount, activity, accountManagerFuture -> {
            mAccount = null;
            if (mContext != null) {
                MainActivity.goToMain(mContext, true);
                Toast.makeText(mContext, mContext.getString(R.string.successful_log_out), Toast.LENGTH_LONG).show();
            }
        }, null);
    }
}
