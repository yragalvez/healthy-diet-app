package com.ycjg.dietapp.data.db.repository;

import com.ycjg.dietapp.data.db.DaoSession;
import com.ycjg.dietapp.data.db.User;
import com.ycjg.dietapp.data.db.UserDao;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by yragalvez on 26/11/2018.
 * DietApp
 */
public class UserRepository {

    private final DaoSession mDaoSession;

    @Inject
    public UserRepository(DaoSession daoSession) {
        mDaoSession = daoSession;
    }

    public Observable<Boolean> saveUser(final User user) {
        return Observable.fromCallable(() -> {
            mDaoSession.getUserDao().insertInTx(user);
           return true;
        });
    }

    public Observable<User> getUser(String email) {
        return Observable.fromCallable(() ->
                mDaoSession.getUserDao()
                        .queryBuilder()
                        .where(UserDao.Properties.Email.eq(email))
                        .unique());
    }

    public void loginUser(String email, String password) {
    }

    public Observable<Boolean> updateUser(User user) {
        return Observable.fromCallable(() -> {
            mDaoSession.getUserDao().updateInTx(user);
            return true;
        });
    }
}
