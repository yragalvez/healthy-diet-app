package com.ycjg.dietapp.data.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycjg.dietapp.utils.ModelParser;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.json.JSONObject;

import java.io.IOException;

import static android.util.Log.e;

/**
 * Created by yragalvez on 09/11/2018.
 * DietApp
 */
@Entity()
public class User {

    private static final String TAG = User.class.getSimpleName();

    @Id(autoincrement = true)
    private long id;

    private String firstName;
    private String lastName;
    private String email;
    private String userId;
    private double weight;
    private double height;
    private int weightPlan;

    @Generated(hash = 455659786)
    public User(long id, String firstName, String lastName, String email,
            String userId, double weight, double height, int weightPlan) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.userId = userId;
        this.weight = weight;
        this.height = height;
        this.weightPlan = weightPlan;
    }
    @Generated(hash = 586692638)
    public User() {
    }
    public long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getFirstName() {
        return this.firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getUserId() {
        return this.userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public double getWeight() {
        return this.weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public double getHeight() {
        return this.height;
    }
    public void setHeight(double height) {
        this.height = height;
    }
    public int getWeightPlan() {
        return this.weightPlan;
    }
    public void setWeightPlan(int weightPlan) {
        this.weightPlan = weightPlan;
    }

    public static User parse(JSONObject json) {
        if (json == null)
            return null;
        return new ModelParser<>(User.class).parse(json);
    }

    public static User parse(String json) {
        if (json == null)
            return null;
        try {
            return new ObjectMapper().readValue(json, User.class);
        } catch (IOException ex) {
            e(TAG, "parse()", ex);
            return null;
        }
    }
}
