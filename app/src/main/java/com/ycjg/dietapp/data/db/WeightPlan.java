package com.ycjg.dietapp.data.db;

import android.support.annotation.IntDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by yragalvez on 21/11/2018.
 * DietApp
 */
public class WeightPlan implements Serializable {

    public static final int LOSE_WEIGHT = 1;
    public static final int MAINTAIN_WEIGHT = 2;
    public static final int GAIN_WEIGHT = 3;

    private WeightPlan() {
    }

    @IntDef({LOSE_WEIGHT, MAINTAIN_WEIGHT, GAIN_WEIGHT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Weight {

    }

    public static String getTitle(@Weight int weightPlan) {
        switch (weightPlan) {
            case LOSE_WEIGHT: return "Weight Loss Plan";
            case MAINTAIN_WEIGHT: return "Weight Maintenance Plan";
            case GAIN_WEIGHT: return "Weight Gain Plan";
            default: return "";
        }
    }
}