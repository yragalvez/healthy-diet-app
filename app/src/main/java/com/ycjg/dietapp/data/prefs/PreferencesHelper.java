package com.ycjg.dietapp.data.prefs;

import com.ycjg.dietapp.data.db.WeightPlan;

import javax.inject.Singleton;

/**
 * Created by yragalvez on 12/11/2018.
 * DietApp
 */

@Singleton
public interface PreferencesHelper {

    void setFirstLogin(String email, boolean isFirstLogin);

    boolean isFirstLogin(String email);

    void setCurrentUserName(String username);

    String getCurrentUserName();

    void setCurrentUserEmail(String email);

    String getCurrentUserEmail();

    void setCurrentUserProfilePicUrl(String profilePicUrl);

    String getCurrentUserProfilePicUrl();

    void setUserWeight(String email, double weight);

    String getUserWeight(String email);

    void setUserHeight(String email, double height);

    String getUserHeight(String email);

    void setIsImperial(String email, boolean isImperial);

    boolean getIsImperial(String email);

    void setUserPlan(String email, @WeightPlan.Weight int i);

    int getUserPlan(String email);
}
