package com.ycjg.dietapp.data.account_manager;

import android.app.Activity;
import android.content.Intent;

import com.ycjg.dietapp.data.db.User;

import javax.inject.Singleton;

/**
 * Created by yragalvez on 13/11/2018.
 * DietApp
 */
@Singleton
public interface AccountManagerHelper {
    boolean isLoggedId();

    void authenticateActivity(Activity activity);

    User getUserData();

    Intent addAccount(User user, String password);

    void logout(Activity activity);
}
